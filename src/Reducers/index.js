import transactionsListReducer from "./transactionsListReducer";
import { combineReducers } from "redux";

const Reducer = combineReducers({
  transactionsList: transactionsListReducer
});

export default Reducer;
