let txId = 0;

function transactionsListReducer(state = [], action) {
  switch (action.type) {
    case "addTransaction":
      return [...state, { ...action.payload.transaction, id: ++txId }];
    case "setTransactionList":
      return [...state.filter(tx => tx.id !== action.payload.transactionId)];
    case "editTransaction":
      const tx = [...state].find(tx => tx.id === action.payload.transaction.id);
      tx.date = action.payload.transaction.date;
      tx.description = action.payload.transaction.description;
      tx.transactionType = action.payload.transaction.transactionType;
      tx.transactionAmount = action.payload.transaction.transactionAmount;
      return [...state];
    default:
      return state;
  }
}

export default transactionsListReducer;
