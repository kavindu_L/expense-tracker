import { CircularProgressbar } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import { useState } from "react";
import { BsDot } from "react-icons/bs";

import "./body.css";
import Left from "../Left/Left";
import Right from "../Right/Right";
import Popup from "../Popup/Popup";

function Body() {
  const [openPopup, setOpenPopup] = useState(false);
  return (
    <div className="et-body-wrapper">
      <div className="et-left-side">
        <Left />
      </div>
      <div className="et-center">
        <div className="et-chart">
          <span className="et-chart-title">September Tracker</span>
          <div className="et-progress-bar">
            <CircularProgressbar
              value={75}
              text={`${75}%`}
              counterClockwise={true}
            />
          </div>
          <p className="et-sub-title">You have spent 25% of your income</p>
        </div>
        <div className="et-center-dashboards">
          <div className="et-details-dashboard">
            <div className="et-details-txt">
              <BsDot style={{ fontSize: "32px", color: "#6464df" }} />
              Income
            </div>
            <div className="et-details-value" style={{ color: "#6464df" }}>
              $12,000
            </div>
          </div>
          <div className="et-details-dashboard">
            <div className="et-details-txt">
              <BsDot style={{ fontSize: "32px", color: "#f74c4c" }} />
              Expenses
            </div>
            <div className="et-details-value" style={{ color: "#f74c4c" }}>
              $3,000
            </div>
          </div>
          <div className="et-details-dashboard">
            <div className="et-details-txt">
              <BsDot style={{ fontSize: "32px", color: "#0eb90e" }} />
              Savings
            </div>
            <div className="et-details-value" style={{ color: "#0eb90e" }}>
              $9,000
            </div>
          </div>
        </div>

        <div
          className="et-add-new-btn"
          onClick={() => {
            setOpenPopup(true);
          }}
        >
          Add New
        </div>
      </div>

      <div className="et-right-side">
        <Right setOpenPopup={setOpenPopup} />
      </div>
      {openPopup && <Popup setOpenPopup={setOpenPopup} />}
    </div>
  );
}

export default Body;
