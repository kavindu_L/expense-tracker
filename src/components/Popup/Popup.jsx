import { AiFillCloseCircle } from "react-icons/ai";

import "./popup.css";

function Popup({ setOpenPopup }) {
  function handleSubmit() {
    setOpenPopup(false);
  }

  return (
    <>
      <div className="et-popup-overlay"></div>
      <div className="et-popup-content">
        <span
          className="et-popup-close-btn"
          onClick={() => setOpenPopup(false)}
        >
          <AiFillCloseCircle style={{ fontSize: "24px" }} />
        </span>
        <form className="et-popup-form" onSubmit={handleSubmit}>
          <div className="et-popup-form-input">
            <label className="et-popup-form-label" htmlFor="description">
              Date
            </label>
            <input type="date" />
          </div>

          <div className="et-popup-form-input">
            <label className="et-popup-form-label" htmlFor="amount">
              Description
            </label>
            <input htmlFor="description" type="text" />
          </div>

          <div className="et-popup-form-input">
            <label className="et-popup-form-label" htmlFor="amount">
              Amount
            </label>
            <input htmlFor="amount" type="number" />
          </div>

          <button className="et-popup-form-submit-btn" type="submit">
            Add
          </button>
        </form>
      </div>
    </>
  );
}

export default Popup;
