import { FcDoughnutChart } from "react-icons/fc";
import { TiThMenu } from "react-icons/ti";

import "./header.css";

function Header() {
  return (
    <div className="et-header-wrapper">
      <div className="et-header-left">
        <div className="et-header-icon">
          <FcDoughnutChart style={{ width: "100%", height: "100%" }} />
        </div>
        <div className="et-header-name">
          <span>Expense Tracker</span>
        </div>
      </div>
      <div className="et-header-right">
        <span className="et-header-hello-txt">
          Hello,{" "}
          <i>
            <strong>John</strong>
          </i>
        </span>
        <div className="et-header-avatar">
          <img
            className="et-header-avatar-img"
            src="https://mui.com/static/images/avatar/2.jpg"
            alt="avatar"
          />
        </div>
        <div className="et-header-menu-icon">
          <TiThMenu style={{ width: "100%", height: "100%" }} />
        </div>
      </div>
    </div>
  );
}

export default Header;
