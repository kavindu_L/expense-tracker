import { BsCardList } from "react-icons/bs";
import { AiOutlineMore } from "react-icons/ai";

import "./right.css";

function Right({ setOpenPopup }) {
  return (
    <div className="et-right-wrapper">
      <div className="et-right-topic">
        <div className="et-right-topic-icon">
          <BsCardList />
        </div>
        <div className="et-right-topic-text">Last Transactions</div>
      </div>
      <div className="et-transactions-list">
        <div className="et-transaction">
          <div className="et-tx-desccription">
            <div className="et-tx-date">9/1</div>
            <div className="et-txdescription-txt">Tip for last service</div>
          </div>
          <div className="et-tx-amount">
            <div className="et-tx-amount-val et-income">$5,000</div>
            <div className="et-tx-more-icon">
              <AiOutlineMore />
            </div>
          </div>
        </div>
        <div className="et-transaction">
          <div className="et-tx-desccription">
            <div className="et-tx-date">9/2</div>
            <div className="et-txdescription-txt">Bought new TV</div>
          </div>
          <div className="et-tx-amount">
            <div className="et-tx-amount-val et-expence">$600</div>
            <div className="et-tx-more-icon">
              <AiOutlineMore />
            </div>
          </div>
        </div>
        <div className="et-transaction">
          <div className="et-tx-desccription">
            <div className="et-tx-date">9/5</div>
            <div className="et-txdescription-txt">Bonus from office</div>
          </div>
          <div className="et-tx-amount">
            <div className="et-tx-amount-val et-income">$7,000</div>
            <div className="et-tx-more-icon">
              <AiOutlineMore />
            </div>
          </div>
        </div>
        {/* <div className="et-transaction">
          <div className="et-tx-desccription">
            <div className="et-tx-date">9/14</div>
            <div className="et-txdescription-txt">Bill payments</div>
          </div>
          <div className="et-tx-amount">
            <div className="et-tx-amount-val et-expence">$400</div>
            <div className="et-tx-more-icon">
              <AiOutlineMore />
            </div>
          </div>
        </div> */}
        <div className="et-transaction">
          <div className="et-tx-desccription">
            <div className="et-tx-date">9/18</div>
            <div className="et-txdescription-txt">Building repair</div>
          </div>
          <div className="et-tx-amount">
            <div className="et-tx-amount-val et-expence">$2,400</div>
            <div className="et-tx-more-icon">
              <AiOutlineMore />
            </div>
          </div>
        </div>
      </div>
      <div className="et-add-new-bar" onClick={() => setOpenPopup(true)}>
        Add New
      </div>
    </div>
  );
}

export default Right;
