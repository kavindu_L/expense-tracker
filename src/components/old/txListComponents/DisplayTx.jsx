import DeleteButton from "./DeleteButton";
import { txAmountParser } from "../../Functions";
import { TiEdit } from "react-icons/ti";

function DisplayTx({ tx, handleEdit }) {
  return (
    <div
      className={
        "rounded p-1 mb-1 "
        // (tx.transactionType === "income" ? "bg-warning" : "")
      }
      style={{
        backgroundColor: tx.transactionType === "income" ? "#fffa93" : ""
      }}
    >
      <div className="d-flex justify-content-between">
        <div>{tx.description}</div>
        <div className="d-flex justify-content-between">
          Rs.{txAmountParser(tx.transactionAmount)}
          <div className="ms-1">
            <TiEdit onClick={handleEdit} />
          </div>
          <div>
            <DeleteButton txId={tx.id} />
          </div>
        </div>
      </div>
    </div>
  );
}

export default DisplayTx;
