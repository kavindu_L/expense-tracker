import Tx from "./Tx";

function dateParser(date) {
  const dateString = date;
  const dateSplitter = dateString.split("-");

  return `${dateSplitter[1]}/${dateSplitter[2]}`;
}

function TxListForADay({ dateKey, txList }) {
  // sort by transaction type  [incomes first]
  txList.sort((a, b) => (b.transactionType === "income" ? 1 : -1));

  return (
    <div className="bg-white rounded p-3 mb-3">
      <h3>{dateParser(dateKey)}</h3>
      {txList.map(tx => (
        <Tx tx={tx} key={tx.id} />
      ))}
    </div>
  );
}

export default TxListForADay;
