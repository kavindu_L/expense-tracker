import { useDispatch } from "react-redux";
import * as transactionsListActions from "../../Actions/transactionsListActions";

import { TiDeleteOutline } from "react-icons/ti";

function DeleteButton({ txId }) {
  const dispatch = useDispatch();

  function handleDeleteButton() {
    let text = "Are you sure about the deleting this item?";

    if (window.confirm(text) === true) {
      dispatch(transactionsListActions.setTransactionList(txId));
    }
  }

  return (
    <div className="ms-1">
      <TiDeleteOutline onClick={() => handleDeleteButton()} />
    </div>
  );
}

export default DeleteButton;
