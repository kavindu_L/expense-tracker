import { useState } from "react";
import DisplayTx from "./DisplayTx";
import EditTx from "./EditTx";

function Tx({ tx }) {
  const [isEditEnable, setIsEditEnable] = useState(false);

  function handleEdit() {
    setIsEditEnable(!isEditEnable);
  }

  return (
    <>
      {isEditEnable ? (
        <EditTx tx={tx} handleEdit={handleEdit} />
      ) : (
        <DisplayTx tx={tx} handleEdit={handleEdit} />
      )}
    </>
  );
}

export default Tx;
