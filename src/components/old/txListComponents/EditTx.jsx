import { useState } from "react";
import { useDispatch } from "react-redux";
import * as transactionsListActions from "../../Actions/transactionsListActions";

function EditTx({ tx, handleEdit }) {
  const dispatch = useDispatch();

  const [date, setDate] = useState(tx.date);
  const [description, setDesciption] = useState(tx.description);
  const [transactionType, setTranactionType] = useState(tx.transactionType);
  const [transactionAmount, setTransactionAmount] = useState(
    tx.transactionAmount
  );

  function handleFormSubmission(event) {
    event.preventDefault();

    dispatch(
      transactionsListActions.editTransaction({
        id: tx.id,
        date,
        description,
        transactionType,
        transactionAmount
      })
    );

    setDate("");
    setDesciption("");
    setTranactionType("");
    setTransactionAmount(0);

    handleEdit();
  }

  return (
    <div>
      <form onSubmit={handleFormSubmission}>
        <div className="input-group input-group-sm">
          <input
            required
            type="date"
            className="form-control"
            value={date}
            onChange={e => setDate(e.target.value)}
          />

          <input
            required
            type="text"
            placeholder="type a description"
            className="form-control"
            value={description}
            onChange={e => setDesciption(e.target.value)}
          />

          <select
            required
            className="form-select"
            value={transactionType}
            onChange={e => setTranactionType(e.target.value)}
          >
            <option value="">Choose the expense type</option>
            <option value="income">income</option>
            <option value="expense">expense</option>
          </select>
          <span className="input-group-text">Rs.</span>
          <input
            required
            type="number"
            placeholder="type the amount"
            className="form-control"
            value={transactionAmount}
            onChange={e => setTransactionAmount(e.target.value)}
          />
          <input type="submit" className="btn btn-primary" value="save" />
        </div>
      </form>
      {/* <button onClick={handleEdit}>save</button> */}
    </div>
  );
}

export default EditTx;
