import { RiDashboardLine } from "react-icons/ri";
import { MdPayment } from "react-icons/md";
import { BiLineChart } from "react-icons/bi";
import { FiHelpCircle, FiSettings } from "react-icons/fi";

import "./left.css";

function Left() {
  return (
    <div className="et-left-wrapper">
      <div className="et-left-link">
        <div className="et-link-icon">
          <RiDashboardLine
            style={{ width: "25px", height: "25px", color: "black" }}
          />
        </div>
        <div className="et-link-text active-txt">Dashboard</div>
      </div>
      <div className="et-left-link">
        <div className="et-link-icon">
          <MdPayment style={{ width: "25px", height: "25px", color: "gray" }} />
        </div>
        <div className="et-link-text">Bill payments</div>
      </div>
      <div className="et-left-link">
        <div className="et-link-icon">
          <BiLineChart
            style={{ width: "25px", height: "25px", color: "gray" }}
          />
        </div>
        <div className="et-link-text">Expences</div>
      </div>
      <div className="et-left-link">
        <div className="et-link-icon">
          <FiSettings
            style={{ width: "25px", height: "25px", color: "gray" }}
          />
        </div>
        <div className="et-link-text">Settings</div>
      </div>
      <div className="et-left-link">
        <div className="et-link-icon">
          <FiHelpCircle
            style={{ width: "25px", height: "25px", color: "gray" }}
          />
        </div>
        <div className="et-link-text">Help</div>
      </div>
    </div>
  );
}

export default Left;
