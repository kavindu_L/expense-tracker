export const addTransaction = transaction => {
  return {
    type: "addTransaction",
    payload: {
      transaction
    }
  };
};

export const editTransaction = transaction => {
  return {
    type: "editTransaction",
    payload: {
      transaction
    }
  };
};

export const setTransactionList = transactionId => {
  return {
    type: "setTransactionList",
    payload: {
      transactionId
    }
  };
};
