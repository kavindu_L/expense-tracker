import InputField from "./InputField";
import TransactionsList from "./TransactionsList";
import Visualization from "./Visualization";
import { useSelector } from "react-redux";

function ExpenseTracker() {
  const transactionsList = useSelector(state => state.transactionsList);
  return (
    <div className="container mt-3">
      <h2>Expence Tracker</h2>
      <InputField />

      {transactionsList.length ? (
        <div className="d-flex justify-content-between">
          <div
            className="w-50 mt-3 me-3 align-self-start overflow-auto"
            style={{ height: "500px" }}
          >
            <TransactionsList />
          </div>
          <div
            className="w-50 mt-3 align-self-start overflow-auto"
            style={{ height: "500px" }}
          >
            <Visualization />
          </div>
        </div>
      ) : (
        ""
      )}
    </div>
  );
}

export default ExpenseTracker;
