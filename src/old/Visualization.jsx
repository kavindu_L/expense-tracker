import { useSelector } from "react-redux";
import { CircularProgressbar } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import { txAmountParser } from "./Functions";

function Visualization() {
  const transactionsList = useSelector(state => state.transactionsList);

  const incomeList = transactionsList
    .filter(tx => tx.transactionType === "income")
    .map(tx => Number(tx.transactionAmount));

  const expenseList = transactionsList
    .filter(tx => tx.transactionType === "expense")
    .map(tx => Number(tx.transactionAmount));

  const totalIncome = incomeList.reduce((a, b) => a + b, 0);
  const totalExpense = expenseList.reduce((a, b) => a + b, 0);

  let incomeWidth = 0;
  let remainingWidth = 0;

  if (totalIncome > totalExpense) {
    incomeWidth = 100;
    remainingWidth = ((totalIncome - totalExpense) / totalIncome) * 100;
  } else if (remainingWidth === 0 && incomeWidth === 0) {
    remainingWidth = 0;
    incomeWidth = 0;
  } else {
    remainingWidth = 100;
    incomeWidth = ((totalExpense - totalIncome) / totalExpense) * 100;
  }

  const percentage = Number(remainingWidth.toFixed(1));
  const progressbarStyles = {
    text: { fontSize: "14px" }
  };

  const walletBalanace = totalIncome - totalExpense;

  return (
    <div className="p-3 bg-light rounded">
      {/* <h3 className="sticky-top bg-light">Visualization</h3> */}

      <center className="mt-3">
        <div className="mb-3" style={{ width: 250, height: "auto" }}>
          <CircularProgressbar
            value={percentage}
            text={`${percentage}%`}
            styles={progressbarStyles}
            counterClockwise={true}
          />
          <span className="mt-3">
            <i>total remaining income</i>
          </span>
        </div>

        <div className="d-flex justify-content-between">
          <div>
            <h3>
              <span className="badge bg-info">
                {txAmountParser(totalIncome.toString())}
              </span>
            </h3>
            <span>
              <i>income</i>
            </span>
          </div>

          <div>
            <h3>
              <span className="badge bg-info">
                {txAmountParser(totalExpense.toString())}
              </span>
            </h3>
            <span>
              <i>expenditure</i>
            </span>
          </div>

          <div>
            <h3>
              <span
                className={
                  "badge bg-" + (walletBalanace > 0 ? "info" : "danger")
                }
              >
                {txAmountParser(walletBalanace.toString())}
              </span>
            </h3>
            <span>
              <i>balance</i>
            </span>
          </div>
        </div>
      </center>
    </div>
  );
}

export default Visualization;
