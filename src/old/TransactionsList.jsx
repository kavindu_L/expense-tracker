import { useSelector } from "react-redux";
import TxListForADay from "./components/txListComponents/TxListForADay";
import { groupBy } from "./Functions";

function TransactionsList() {
  const transactionsList = useSelector(state => state.transactionsList);
  transactionsList.sort((a, b) => (a.date > b.date ? 1 : -1));

  const groupBydate = groupBy(transactionsList, "date");
  const keys = Object.keys(groupBydate);

  return (
    <div className="p-3 bg-light rounded">
      {/* <h3 className="sticky-top bg-light">Transactions List</h3> */}

      {keys.map((key, index) => (
        <div key={index}>
          <TxListForADay dateKey={key} txList={groupBydate[key]} />
        </div>
      ))}

      {/* {transactionsList.map(tx => (
        <div key={tx.id}>
          {`${tx.date} : ${tx.description} => Rs. ${tx.transactionAmount}`}
          <hr />
        </div>
      ))} */}
    </div>
  );
}

export default TransactionsList;
