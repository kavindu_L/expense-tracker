export function txAmountParser(amountStr) {
  amountStr = amountStr.toString();

  if (amountStr === "0") {
    return amountStr;
  }

  if (amountStr.startsWith("0")) {
    amountStr = amountStr.slice(1);
  }

  const reversedAmountList = amountStr.split("").reverse();
  let newAmountStr = "";
  let startIndex = 0;

  for (let i = 3; i < reversedAmountList.length + 3; i += 3) {
    newAmountStr += reversedAmountList.slice(startIndex, i).join("");
    newAmountStr += ",";
    startIndex = i;
  }

  if (newAmountStr.endsWith(",")) {
    newAmountStr = newAmountStr.slice(0, -1);
  }

  return newAmountStr
    .split("")
    .reverse()
    .join("");
}

// takes an array of objects and the name of a property of these objects
export const groupBy = (array, property) =>
  array.reduce(
    (grouped, element) => ({
      ...grouped,
      [element[property]]: [...(grouped[element[property]] || []), element]
    }),
    {}
  );
