import { useDispatch } from "react-redux";
import * as transactionsListActions from "./Actions/transactionsListActions";
import { useState } from "react";

function InputField() {
  const dispatch = useDispatch();

  const [date, setDate] = useState("");
  const [description, setDesciption] = useState("");
  const [transactionType, setTranactionType] = useState("");
  const [transactionAmount, setTransactionAmount] = useState(0);

  function handleFormSubmission(event) {
    event.preventDefault();
    dispatch(
      transactionsListActions.addTransaction({
        date,
        description,
        transactionType,
        transactionAmount
      })
    );

    setDate("");
    setDesciption("");
    setTranactionType("");
    setTransactionAmount(0);
  }

  return (
    <center className="mt-3">
      {/* Input Field */}
      <form onSubmit={handleFormSubmission}>
        <div className="input-group">
          <input
            required
            type="date"
            className="form-control"
            value={date}
            onChange={e => setDate(e.target.value)}
          />

          <input
            required
            type="text"
            placeholder="type a description"
            className="form-control"
            value={description}
            onChange={e => setDesciption(e.target.value)}
          />

          <select
            required
            className="form-select"
            value={transactionType}
            onChange={e => setTranactionType(e.target.value)}
          >
            <option value="">Choose the expense type</option>
            <option value="income">income</option>
            <option value="expense">expense</option>
          </select>
          <span className="input-group-text">Rs.</span>
          <input
            required
            type="number"
            placeholder="type the amount"
            className="form-control"
            value={transactionAmount}
            onChange={e => setTransactionAmount(e.target.value)}
          />
          <input type="submit" className="btn btn-primary" value="Add" />
        </div>
      </form>
    </center>
  );
}

export default InputField;
