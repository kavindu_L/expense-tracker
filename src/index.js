import React from "react";
import ReactDOM from "react-dom/client";
import ExpenseTracker from "./ExpenseTracker";

// import { Provider } from "react-redux";
// import { createStore } from "redux";
// import Reducer from "./Reducers";

const root = ReactDOM.createRoot(document.getElementById("root"));
// const store = createStore(Reducer);

root.render(
  // <Provider store={store}>
  <React.StrictMode>
    <ExpenseTracker />
  </React.StrictMode>
  // </Provider>
);
