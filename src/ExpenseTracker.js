import Header from "./components/Header/Header";
import Body from "./components/Body/Body";

import "./index.css";

function ExpenseTracker() {
  return (
    <div className="container mt-3">
      <Header />
      <Body />
      {/* <h2>Expence Tracker</h2> */}
    </div>
  );
}

export default ExpenseTracker;
